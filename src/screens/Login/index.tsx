import React from "react";
import { Image, View, Text, TextInput, TouchableOpacity } from "react-native";
import { useForm, Controller } from 'react-hook-form';
import { useNavigation } from "@react-navigation/core";
import { BackGroundApp, InputForm, Button, TextButton, LogoApp, TextForm, ViewForm, ViewForm2, GoToLogin } from "./styles"

export default function Cadastro() {

    const navigation = useNavigation();

    const { control, handleSubmit, formState:{errors}, getValues } = useForm({mode:'onTouched'});

    const onSubmit = (data: FormData) => {
        console.log(data);
    }

    interface FormData {
        email: string;
        senha: string;
    }

    return (
        <BackGroundApp>

            <ViewForm2>
                <LogoApp source={require('../../../assets/LOGO.png')}></LogoApp>
            </ViewForm2>

            <ViewForm>

                {/* EMAIL */}
                <View>
                    <TextForm>E-mail</TextForm> 
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <InputForm
                                placeholder='example@example.com'
                                maxLength={256}
                                onBlur={onBlur}
                                onChangeText= {(value:any) => onChange(value.trim())}
                                value={value.trim()}          
                            />
                        )}
                        rules={{ 
                            required: 'O e-mail é obrigatório',
                            pattern: {
                                value: /^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: 'Formato de e-mail inválido'
                            },
                        }}
                        name='email'
                        defaultValue=''
                    />
                    {errors.email && <Text style={{ color:'#D65252'}}>{errors.email.message}</Text>}
                </View>

                {/* SENHA */}
                <View>
                    <TextForm>Senha</TextForm> 
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <InputForm
                                placeholder='••••••'
                                maxLength={18}
                                secureTextEntry
                                onBlur={onBlur}
                                onChangeText= {(value:any) => onChange(value.trim())}
                                value={value.trim()}          
                            />
                        )}
                        rules={{ 
                            required: 'A senha é obrigatória',
                            minLength: {
                                value: 6,
                                message: 'Senha muito curta'
                            },
                        }}
                        name='senha'
                        defaultValue=''
                    /> 
                    {errors.senha && <Text style={{ color:'#D65252'}}>{errors.senha.message}</Text>}
                </View>

                <Button onPress={handleSubmit(onSubmit)}>
                    <TextButton>Entrar</TextButton>
                </Button>

                <TouchableOpacity onPress={() => navigation.navigate("Cadastro")}>
                        <GoToLogin>Novo aqui? Crie sua conta agora!</GoToLogin>
                </TouchableOpacity>

            </ViewForm>


        </BackGroundApp>
    )
}