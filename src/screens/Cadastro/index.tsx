import React from "react";
import { Image, View, Text, TextInput, TouchableOpacity } from "react-native";
import { useForm, Controller } from 'react-hook-form';
import { useNavigation } from '@react-navigation/native';
import { BackGroundApp, NomeForm, InputForm, Button, TextButton, LogoApp, ViewNome, 
         InputFormNome, InputFormSobrenome, TextForm, TextForm2, ViewForm, ViewForm2, GoToLogin } from "./styles"

export default function Cadastro() {

    const navigation = useNavigation();

    const { control, handleSubmit, formState:{errors}, getValues } = useForm({mode:'onTouched'});

    const onSubmit = (data: FormData) => {
        console.log(data);
    }

    interface FormData {
        nome: string;
        sobrenome: string;
        usuario: string;
        email: string;
        senha: string;
        confirmarSenha: string;
    }

    return (
        <BackGroundApp>

            <ViewForm2>
                <LogoApp source={require('../../../assets/LOGO.png')}></LogoApp>
                <TextForm2>Cadastre-se para encontrar <br/> os melhores produtos <br/>eletrônicos!</TextForm2>
            </ViewForm2>

            <ViewForm>

                {/* NOME */}
                <ViewNome>
                    <View>
                        <NomeForm>Nome</NomeForm>
                        <Controller
                            control={control}
                            render={({field:{onBlur, onChange, value}}) => (
                                <InputFormNome
                                    placeholder='ex.: Maria'
                                    maxLength={35}
                                    onBlur={onBlur}
                                    onChangeText= {(value:any) => onChange(value.trim())}
                                    value={value.trim()}
                                />
                            )}
                            rules={{ 
                                required: 'O nome é obrigatório',
                                pattern: {
                                    value: /^[a-zA-Z0-9]+$/i,
                                    message: 'Somente letras e números permitidos'
                                },
                            }}
                            name='nome'
                            defaultValue=''
                        />
                        {errors.nome && <Text style={{ color:'#D65252'}}>{errors.nome.message}</Text>}
                        
                    </View>

                    {/* SOBRENOME */}
                    <View>
                        <NomeForm>Sobrenome</NomeForm>
                        <Controller
                            control={control}
                            render={({field:{onBlur, onChange, value}}) => (
                                <InputFormSobrenome
                                    placeholder='ex.: Silva'
                                    maxLength={35}
                                    onBlur={onBlur}
                                    onChangeText= {(value:any) => onChange(value.trim())}
                                    value={value.trim()}          
                                />
                            )}
                            rules={{ 
                                required: 'O sobrenome é obrigatório',
                                pattern: {
                                    value: /^[a-zA-Z0-9]+$/i,
                                    message: 'Somente letras e números permitidos'
                                },
                            }}
                            name='sobrenome'
                            defaultValue=''
                        />
                        {errors.sobrenome && <Text style={{ color:'#D65252'}}>{errors.sobrenome.message}</Text>}
                    </View>
                </ViewNome>

                {/* USUARIO */}
                <View>
                    <TextForm>Usuário</TextForm>  
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <InputForm
                                placeholder='Example123'
                                maxLength={30}
                                onBlur={onBlur}
                                onChangeText= {(value:any) => onChange(value.trim())}
                                value={value.trim()}          
                            />
                        )}
                        rules={{ 
                            required: 'O usuário é obrigatório',
                            minLength: {
                                value: 3,
                                message: 'Usuário inválido'
                            },
                            pattern: {
                                value: /^[a-zA-Z0-9]+[a-zA-Z0-9._-]+$/i,
                                message: 'Usuário inválido'
                            },
                        }}
                        name='usuario'
                        defaultValue=''
                    />
                    {errors.usuario && <Text style={{ color:'#D65252'}}>{errors.usuario.message}</Text>}
                </View>

                {/* EMAIL */}
                <View>
                    <TextForm>E-mail</TextForm> 
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <InputForm
                                placeholder='example@example.com'
                                maxLength={256}
                                onBlur={onBlur}
                                onChangeText= {(value:any) => onChange(value.trim())}
                                value={value.trim()}          
                            />
                        )}
                        rules={{ 
                            required: 'O e-mail é obrigatório',
                            pattern: {
                                value: /^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: 'Formato de e-mail inválido'
                            },
                        }}
                        name='email'
                        defaultValue=''
                    />
                    {errors.email && <Text style={{ color:'#D65252'}}>{errors.email.message}</Text>}
                </View>

                {/* SENHA */}
                <View>
                    <TextForm>Senha</TextForm> 
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <InputForm
                                placeholder='••••••'
                                maxLength={18}
                                secureTextEntry
                                onBlur={onBlur}
                                onChangeText= {(value:any) => onChange(value.trim())}
                                value={value.trim()}          
                            />
                        )}
                        rules={{ 
                            required: 'A senha é obrigatória',
                            minLength: {
                                value: 6,
                                message: 'Senha muito curta'
                            },
                        }}
                        name='senha'
                        defaultValue=''
                    /> 
                    {errors.senha && <Text style={{ color:'#D65252'}}>{errors.senha.message}</Text>}
                </View>

                {/* CONFIRMAR SENHA */}
                <View>
                    <TextForm>Confirme sua senha</TextForm> 
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <InputForm
                                placeholder='••••••'
                                maxLength={18}
                                secureTextEntry
                                onBlur={onBlur}
                                onChangeText= {(value:any) => onChange(value.trim())}
                                value={value.trim()}          
                            />
                        )}
                        rules={{ 
                            validate: {
                            comparaSenhas: (value) => {
                            const { senha } = getValues();
                            return senha === value || 'As senhas não coincidem';
                            }
                            }
                        }}
                        name='confirmarSenha'
                        defaultValue=''
                    />
                    {errors.confirmarSenha && <Text style={{ color:'#D65252'}}>{errors.confirmarSenha.message}</Text>}
                </View>

                <Button onPress={handleSubmit(onSubmit)}>
                    <TextButton>Enviar</TextButton>
                </Button>
                

                <TouchableOpacity onPress={() => navigation.navigate("Login")}>
                        <GoToLogin>Já possui cadastro? Faça seu login aqui</GoToLogin>
                </TouchableOpacity>

            </ViewForm>


        </BackGroundApp>
    )
}