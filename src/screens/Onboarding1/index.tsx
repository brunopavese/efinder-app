import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import { useForm } from 'react-hook-form';
import { useNavigation } from '@react-navigation/native';
import { ImageBoard, BackGroundApp, ViewBoard, ViewBoard2, ViewBoard3, Button, TextButton,
         TextBoard1, TextBoard2, ImageDots } from './styles';


export default function Onboarding1() {

    const navigation = useNavigation();

    const { control } = useForm({mode:'onTouched'});
    
    return (
        <BackGroundApp>
            <ViewBoard>
            <ImageBoard source={require('../../../assets/Onboarding1.png')}></ImageBoard>
            </ViewBoard>
            <ViewBoard2>
                <ViewBoard3>
                    <TextBoard1>Compre Aqui</TextBoard1>
                    <TextBoard2>Encontre tudo o que você precisa <br/>de produtos eletrônicos de maneira<br/>rápida e segura</TextBoard2>
                </ViewBoard3>
                <ImageDots source={require('../../../assets/dots1.png')}></ImageDots>
                <Button onPress={() => navigation.navigate("Onboarding2")}>
                    <TextButton>Próximo</TextButton>
                </Button>                
            </ViewBoard2>



        </BackGroundApp>



    )


}